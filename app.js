var http = require('http');
var fs = require('fs');

/*
var yonlendirici = new Object();
var homeController = function (req,res) {

}

var loginController = function (req,res) {

}

yonlendirici['/'] = homeController;
yonlendirici['/login'] = loginController;

var server = http.createServer(function (req, res) {

    if(req.url in yonlendirici){
        yonlendirici[req.url](req,res);
    }
});

server.listen(8004);
*/

var express = require('express');
var path = require('path');
var app = express();
var objectElektronik = require('./ElektronikController');
app.use('/public', express.static(path.join((__dirname, 'public'))));

app.get('/elektronik', objectElektronik.index);
app.get('/elektronik/bilgisayar', objectElektronik.bilgisayar);

/*
app.get('/', function (req, res) {
    fs.readFile('index.html', function (err, data) {
        res.write(data);
        res.end('Mesaj bitti.');
        console.log('HOME');
    });
});

app.get('/login', function (req, res) {
    fs.readFile('login.html', function (err, data) {
        console.log('LOGIN ');
        res.write(data);
        res.end('Mesaj bitti.');
    });
});*/

app.listen(8004);